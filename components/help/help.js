document.querySelectorAll('.webform__help').forEach((button) => {
    const dialog = button.nextElementSibling;

    button.addEventListener('click', (evt) => {
        if(dialog.open) {
            dialog.close();
            return;
        }
        dialog.show();
    });

    const close = dialog.querySelector('.webform-help__close');
    if (close) {
        close.addEventListener('click', () => {
            dialog.close();
        })
    }
});

